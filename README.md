# Birthquake

## Development setup

Birthquake uses the Yarn package manager for development. Make sure to install
Yarn on your development workstation. See the their website for instructions:

https://yarnpkg.com

Use the following commands to install dependencies and launch the Birthquake
locally for development:

```
git clone https://crstephenson@bitbucket.org/crstephenson/birthquake.git
cd birthquake
yarn install
yarnpkg start
```

## Production deployment

To build this app for production, first set the intended homepage URL in
`package.json`. For example:

```
{
  ...
  "homepage": "http://www.earthscope.org/birthquake",
  ...
}
```

The build process will use this to determine relative URLs for website files.

Then run the following commands to build the web app for deployment:

```
cd birthquake
yarn build
```

This will create a `build` subdirectory. Copy this directory to your public
web server and rename it to make sure its URL matches the location you have
specified in `package.json`.
